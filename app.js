// Welcome to my website
var express = require('express');
var app = express();
var favicon = require('serve-favicon');

port = 3000;

app.use(express.static('public'));
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.get("/", (req, res) => {
    res.sendFile(__dirname +'/views/index.html');
});

app.listen(port, () => console.log("Server started"))